
fn main() -> Result<(), Box<dyn std::error::Error>> {
    tonic_build::configure()
        .build_server(false)
        .compile(
            &[
                "proto/falco/version.proto",
                "proto/falco/outputs.proto"
            ],
            &["proto/falco/"],
        )?;

    Ok(())
}