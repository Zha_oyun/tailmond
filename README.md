# tailmond

## 概述

Tailmon主机安全卫士守护进程、终端主程序。

## 源码编译

Rust版本为1.60.0

```shell
git clone https://gitee.com/tailmon-edr/tailmond.git
cd tailmond
cargo build
```


## 项目介绍

Tailmon-EDR项目旨在基于开源软件帮助企业及个人站长构建网络安全监测与防御体系。

目前项目主要开发方向是Linux主机安全软件和多主机统一安全管理中心。

Linux终端主程序采用Rust开发，保证程序自身内存安全性，最大限度降低资源占用以及安全风险。

主要功能点为：

- 网络攻击监控
- 病毒木马扫描
- 漏洞检测
- 应用管理

详细参见：[产品路线图](https://gitee.com/tailmon-edr/docs/blob/master/roadmap/README.md)

> **注意！该项目目前处于非常早期开发阶段，暂不能用于生产环境。新版本发布在这里：[发行版](https://gitee.com/tailmon-edr/tailmond/releases)**


## 下载安装

请点击 **[下载](https://gitee.com/tailmon-edr/tailmond/releases)** 跳转到下载页面。

## 项目文档

请点击 **[文档](https://gitee.com/tailmon-edr/docs/blob/master/README.md)** 跳转到文档页面。

## 产品截图

![](https://gitee.com/tailmon-edr/docs/raw/master/introduction/images/163417_049176e6_1426461.png)

![](https://gitee.com/tailmon-edr/docs/raw/master/introduction/images/163443_9de2888e_1426461.png)

![](https://gitee.com/tailmon-edr/docs/raw/master/introduction/images/163538_40fed834_1426461.png)

## 技术栈

- Rust：Linux终端守护进程
- Go：无需常驻的命令行程序
- Java：安全运营中心
- TypeScript/React：WebUI

## 依赖开源软件

- [Suricata](https://github.com/OISF/suricata)：网络流量监控
- [Falco](https://github.com/falcosecurity/falco)：Linux内核及云原生容器化环境监控
- [Yara](https://github.com/VirusTotal/yara)：恶意软件扫描

## 目录结构

```
- src
- config
    - tailmond.toml
- dist
- lib
- data
- log
- plugins
    - falco
    - ipset
    - suricata
    - tailmonscan
- proto
- migrations
- tmp
- build.rs
- Cargo.toml
- diesel.toml
```

## 新增数据库表

```
export DATABASE_URL="data/local.db"

diesel migration generate create_allow_lists

diesel migration run
```

## 重建数据库

```
diesel database reset
```