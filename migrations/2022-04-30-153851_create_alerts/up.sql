-- Your SQL goes here
CREATE TABLE alerts (
  id VARCHAR NOT NULL PRIMARY KEY,
  severity VARCHAR NOT NULL,
  category VARCHAR NOT NULL,
  description TEXT NOT NULL,
  time BIGINT NOT NULL,
  source VARCHAR NOT NULL,
  mal_obj_type TEXT NOT NULL,
  mal_obj_key TEXT NOT NULL,
  mal_obj_country TEXT NOT NULL,
  mal_obj_city TEXT NOT NULL,
  rule_id VARCHAR NOT NULL,
  solved BOOLEAN NOT NULL DEFAULT 0
)