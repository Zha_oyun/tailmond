
-- Your SQL goes here

CREATE TABLE recovery_lists (
  id VARCHAR NOT NULL PRIMARY KEY,
  mal_obj_key TEXT NOT NULL,
  mal_obj_type TEXT NOT NULL,
  recovery_key TEXT NOT NULL,
  recovery_value TEXT NOT NULL,
  recovery_type TEXT NOT NULL,
  create_time BIGINT NOT NULL, -- 创建时间
  expire_time BIGINT NOT NULL, -- 过期时间。
  source TEXT NOT NULL -- 数据来源
)