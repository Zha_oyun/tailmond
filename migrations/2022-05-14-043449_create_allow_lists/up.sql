-- Your SQL goes here

CREATE TABLE allow_lists (
  id VARCHAR NOT NULL PRIMARY KEY,
  allow_type TEXT NOT NULL, -- File、Directory、IP、IPRange、Executable
  allow_key TEXT NOT NULL, -- 
  create_time BIGINT NOT NULL, -- 创建时间
  expire_time BIGINT NOT NULL, -- 过期时间。
  source TEXT NOT NULL -- 数据来源
)