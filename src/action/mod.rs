


pub mod user_action;
pub mod alert_action;
pub mod allow_list_action;
pub mod recovery_list_action;
pub mod setting_action;

type DbError = Box<dyn std::error::Error + Send + Sync>;