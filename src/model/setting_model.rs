
use serde::{Serialize, Deserialize};
use crate::schema::settings;




/**
 * 设置
 */
#[derive(Debug, Clone, Serialize, Deserialize, Queryable, Insertable, Identifiable, AsChangeset)]
pub struct Setting {

    pub id: String,

    pub value: String,
}

/**
 * 设置
 */
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct SettingDTO {

    pub id: Option<String>,

    pub value: Option<String>,
}