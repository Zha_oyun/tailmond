use std::collections::HashMap;

use serde::{Serialize, Deserialize};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Metadata {
    #[serde(rename = "groupId")]
	pub group_id: String,
    #[serde(rename = "artifactId")]
	pub artifact_id: String,
	pub version: String,
	pub engine: String,
    #[serde(rename = "rulePath")]
	pub rule_path: String,
    pub metadata: HashMap<String, HashMap<String, RuleMeta>>,
}

/**
 * 规则元信息
 */
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct RuleMeta {

    /**
     * 分类
     */
    pub category: String,

    /**
     * 严重程度
     */
    pub severity: String,

    /**
     * 威胁类型
     */
    pub classtype: String,

    /**
     * 规则名称
     */
    pub name: String,

    /**
     * 恶意软件家族
     */
    pub malware_family: String,

    /**
     * 受影响的操作系统
     */
    pub affected_os: String,

    /**
     * 受影响的产品名称
     */
    pub affected_products: String,

    /**
     * 受攻击对象类别
     */
    pub attack_target: String,

    /**
     * 恶意对象类型
     * 例如：IP
     */
    pub mal_obj_type: String,

    /**
     * 恶意对象字段
     * 例如：src_ip
     */
    pub mal_obj_field: String,

    /**
     * 规则创建时间
     */
    pub creation_date: String,

    /**
     * 规则最后一次更新时间
     */
    pub last_modified_date: String,

    /**
     * 规则ID
     */
    pub sid: String,

    /**
     * 规则版本
     */
    pub rev: String,

    /**
     * 规则集
     * 例如：ET、TMT
     */
    pub ruleset: String,

    /**
     * 标记
     */
    pub tag: String,

    /**
     * 高级元信息
     */
    pub advanced: RuleMetaAdvanced,

}

/**
 * 规则高级元信息
 */
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct RuleMetaAdvanced {
    /**
     * 详细描述
     */
    pub description: String,

    /**
     * 废弃原因
     */
    pub deprecation_reason: String,

    /**
     * 性能影响
     */
    pub performance_impact: String,

    /**
     * 规则部署位置
     */
    pub signature_deployment: String,

    /**
     * 参考
     */
    pub reference: Option<Vec<Reference>>,

    /**
     * Mitre攻击阶段
     */
    pub mitre_tags: Option<Vec<MitreTag>>,
}

/**
 * Mitre攻击阶段标记
 */
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct MitreTag {
    pub mitre_tactic_id: String,
    pub mitre_tactic_name: String,
    pub mitre_technique_id: String,
    pub mitre_technique_name: String,
}

/**
 * 规则参考
 */
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Reference {
    /**
     * 参考类型
     * 例如：cve
     */
    #[serde(rename = "type")]
    pub r_type: String,

    /**
     * 参考
     * 例如：CVE-2022-22947
     */
    pub reference: String,
}