use serde::{Serialize, Deserialize};
use crate::schema::recovery_lists;


/**
 * 恢复区
 */
#[derive(Debug, Clone, Serialize, Deserialize, Queryable, Insertable, Identifiable, AsChangeset)]
pub struct RecoveryList {

    pub id: String,

    pub mal_obj_key: String,

    pub mal_obj_type: String,

    pub recovery_key: String,

    pub recovery_value: String,

    pub recovery_type: String,

    pub create_time: i64,

    /**
     * 过期时间
     */
    pub expire_time: i64,

    /**
     * 数据来源
     */
    pub source: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct RecoveryListDTO {

    pub id: Option<String>,

    pub mal_obj_key: Option<String>,

    pub mal_obj_type: Option<String>,

    pub recovery_key: Option<String>,

    pub recovery_value: Option<String>,

    pub recovery_type: Option<String>,

    pub create_time: Option<i64>,

    pub expire_time: Option<i64>,

    pub source: Option<String>,
    
    pub current: Option<i64>,

    #[serde(rename = "pageSize")]
    pub page_size: Option<i64>,
}


#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct RecoveryValue {

    pub mode: Option<u32>,
    
}

