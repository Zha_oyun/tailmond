use serde::{Serialize, Deserialize};
use crate::schema::allow_lists;


/**
 * 白名单
 */
#[derive(Debug, Clone, Serialize, Deserialize, Queryable, Insertable, Identifiable, AsChangeset)]
pub struct AllowList {

    pub id: String,

    /**
     * File、Directory、IP、IPRange、Executable
     */
    pub allow_type: String,

    pub allow_key: String,

    pub create_time: i64,

    /**
     * 过期时间
     */
    pub expire_time: i64,

    /**
     * 数据来源
     */
    pub source: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct AllowListDTO {

    pub id: Option<String>,

    pub allow_type: Option<String>,

    pub allow_key: Option<String>,

    pub create_time: Option<i64>,

    pub expire_time: Option<i64>,

    pub source: Option<String>,
    
    pub current: Option<i64>,

    #[serde(rename = "pageSize")]
    pub page_size: Option<i64>,
}