


/**
 * 暂停进程
 * SIGSTOP
 */
pub fn suspend(pid: i32) {
    unsafe {
        libc::kill(pid, 19);
    }
}

/**
 * 恢复进程
 * SIGCONT
 */
pub fn resume(pid: i32) {
    unsafe {
        libc::kill(pid, 18);
    }
}