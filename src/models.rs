use std::collections::HashMap;

use serde::{Deserialize, Serialize};

//use crate::schema::users;
use crate::schema::alerts;

// #[derive(Debug, Clone, Serialize, Deserialize, Queryable, Insertable)]
// pub struct User {
//     pub id: String,
//     pub name: String,
// }

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct NewUser {
    pub name: String,
}

/**
 * 系统信息
 */
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct SystemInfo {
    pub system: String,
    pub release: String,
    pub version: String,
    pub hostname: String,
    pub arch: String,
    pub virt: String,
    pub memory: u64,
    pub memory_free: u64,
    pub disk: u64,
    pub disk_free: u64,
    pub uptime: f64,
    pub users: usize,
    pub cores: u64,
    pub frequency: u64,
}



/**
 * 告警。
 * 众多Event事件中为Alert告警的事件
 */
#[derive(Debug, Clone, Serialize, Deserialize, Queryable, Insertable, Identifiable, AsChangeset)]
pub struct Alert {

    /**
     * 告警ID。
     * 精确到纳秒的时间格式化字符串。
     * e.g. 
     */
    pub id: String,

    /**
     * 告警严重性
     */
    pub severity: String,

    /**
     * 告警分类
     */
    pub category: String,

    /**
     * 告警描述
     */
    pub description: String,

    /**
     * 告警时间
     */
    pub time: i64,

    /**
     * 告警来源
     */
    pub source: String,

    /**
     * 恶意对象类型
     */
    pub mal_obj_type: String,

    /**
     * 恶意对象标识
     * 当mal_obj_type为IP时，将mal_obj_key当做IP处置；
     * 当mal_obj_type为PROCESS时，将mal_obj_key当做PID处置；
     * 当mal_obj_type为FILE时，将mal_obj_key当做文件路径处置；
     * 当mal_obj_type为DOMAIN时，将mal_obj_key当做域名处置；
     */
    pub mal_obj_key: String,

    /**
     * 恶意对象国家
     * 针对IP类型地理位置
     */
    pub mal_obj_country: String,

    /**
     * 恶意对象城市
     * 针对IP类型地理位置
     */
    pub mal_obj_city: String,

    /**
     * 规则ID
     * 处理一次会话触发多条规则的情况，将产生多条告警信息，告警中带有元数据信息，每条告警只有一个规则ID
     * 这种处置方式可以将规则的触发时间也计算进来。
     */
    pub rule_id: String,

    /**
     * 告警处理状态
     * 0标识未处理、1标识已处理
     * 当关联的所有的恶意对象都不为Unsolved状态时，告警为已处理状态，其他都为未处理状态。
     */
    pub solved: bool,

}

/**
 * 恶意对象
 * Malicious Object
 */
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct MalObj {

    /**
     * ID
     */
    pub id: Option<String>,

    /**
     * 告警关联
     */
    pub alert_id: Option<String>,

    /**
     * 恶意对象类型
     */
    pub mal_obj_type: MalObjType,

    /**
     * 恶意对象标识。
     * 当mal_obj_type为IP时，将mal_obj_key当做IP处置；
     * 当mal_obj_type为PROCESS时，将mal_obj_key当做PID处置；
     * 当mal_obj_type为FILE时，将mal_obj_key当做文件路径处置；
     * 当mal_obj_type为DOMAIN时，将mal_obj_key当做域名处置；
     */
    pub mal_obj_key: String,

    /**
     * 处理状态
     */
    pub solve_status: Option<SolveStatus>,



}

/**
 * 告警严重性
 */
#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum Severity {

    /**
     * 无风险
     */
    None,

    /**
     * 低危
     */
    Low,

    /**
     * 中危
     */
    Medium,

    /**
     * 高危
     */
    High,

    /**
     * 危急
     */
    Critical,
}


/**
 * 告警分类
 */
#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum Category {

    /**
     * 网络攻击
     */
    Attack,

    /**
     * 病毒木马
     */
    Virus,

    /**
     * 系统漏洞
     */
    Vulnerability,

    /**
     * 风险操作
     */
    Risk,

    /**
     * 异常行为
     */
    Anomaly,

    /**
     * 系统异常
     */
    Exception,
}

/**
 * 处理状态
 */
#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum SolveStatus {

    /**
     * 已解决
     */
    Solved,

    /**
     * 强制已解决
     */
    ForceSolved,

    /**
     * 已忽略
     */
    Ignored,

    /**
     * 已还原
     */
    Recovered,

    /**
     * 未解决
     */
    Unsolved,
}

/**
 * 恶意对象类型
 */
#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub enum MalObjType {
    /**
     * 恶意IP
     */
    IP,

    /**
     * 恶意进程
     */
    Process,

    /**
     * 恶意文件。可执行程序、脚本等
     */
    File,

    /**
     * 恶意域名。
     */
    Domain,
}

/**
 * 规则
 */
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Rule {

    /**
     * 规则ID
     */
    pub id: String,

    /**
     * 事件分类
     */
    pub category: String,

    /**
     * 重要程度
     */
    pub severity: String,

    /**
     * 规则描述
     */
    pub description: String,

    /**
     * 高级规则描述字段，提供更详细的描述信息
     */
    pub description_enhance: String,

    /**
     * 是否需要处置
     */
    pub need_solve: bool,

    /**
     * 恶意对象类型
     */
    pub mal_obj_type: String,

    /**
     * 恶意对象提取字段，重要
     */
    pub mal_obj_field: String,

    /**
     * cve编号，没有则空
     */
    pub cve: String,

    /**
     * cpe编号，受影响对象，没有则空
     */
    pub cpe: String,

    /**
     * 当前规则信息语言
     */
    pub lang: String,

    /**
     * 规则给哪个引擎用 suricata、falco、yara
     */
    pub engine: String,

    /**
     * 是否是用户自定义规则
     */
    pub user_defined: bool,

    /**
     * 规则内容。用户自定义规则显示，内置规则不显示
     */
    pub rule_content: String,

}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Metadata {
    #[serde(rename = "groupId")]
	pub group_id: String,
    #[serde(rename = "artifactId")]
	pub artifact_id: String,
	pub version: String,
	pub engine: String,
    #[serde(rename = "rulePath")]
	pub rule_path: String,
    pub metadata: HashMap<String, HashMap<String, RuleMeta>>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct RuleMeta {
	pub category: String,
	pub severity: String,
    #[serde(rename = "type")]
	pub v_type: String,
	pub description: String,
	pub os: String,
	pub reference: Vec<String>,
	pub family: String,
	pub intention: String,
}

/**
 * Yara扫描结果
 */
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct YaraResult {
	pub matches: bool,
    pub timestamp: i64,
	pub mal_obj_type: MalObjType, 
	pub mal_obj_key: String,
	pub rule_id: Option<Vec<String>>,
	pub task_total: i64,
	pub task_finish: i64,
	pub task_match: i64,
}


#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct AlertDTO {
    pub id: Option<String>,
    pub source: Option<String>,
    pub current: i64,
    #[serde(rename = "pageSize")]
    pub page_size: i64,
}

// {"username":"admin","password":"ant.design","autoLogin":true,"type":"account"}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct LoginAccount {
    pub username: String,
    pub password: String,
    #[serde(rename = "autoLogin")]
    pub auto_login: bool,
    #[serde(rename = "type")]
    pub login_type: String,
}

// {"status":"ok","type":"account","currentAuthority":"admin"}
// {"status":"error","type":"account","currentAuthority":"guest"}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct LoginResult {
    pub status: String,
    #[serde(rename = "currentAuthority")]
    pub current_authority: String,
    #[serde(rename = "type")]
    pub login_type: String,
    #[serde(rename = "accessToken", skip_serializing_if = "String::is_empty")]
    pub access_token: String,
}

// {"isLogin":false}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct LoginStatus {
    #[serde(rename = "isLogin")]
    pub is_login: bool,
}

// {"success":true,"data":{"name":"Serati Ma","avatar":"https://gw.alipayobjects.com/zos/antfincdn/XAosXuNZyF/BiazfanxmamNRoxxVxka.png","userid":"00000001","email":"antdesign@alipay.com","signature":"海纳百川，有容乃大","title":"交互专家","group":"蚂蚁金服－某某某事业群－某某平台部－某某技术部－UED","tags":[{"key":"0","label":"很有想法的"},{"key":"1","label":"专注设计"},{"key":"2","label":"辣~"},{"key":"3","label":"大长腿"},{"key":"4","label":"川妹子"},{"key":"5","label":"海纳百川"}],"notifyCount":12,"unreadCount":11,"country":"China","access":"admin","geographic":{"province":{"label":"浙江省","key":"330000"},"city":{"label":"杭州市","key":"330100"}},"address":"西湖区工专路 77 号","phone":"0752-268888888"}}
// {"data":{"isLogin":false},"errorCode":"401","errorMessage":"请先登录！","success":true}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct SysResult<T> {
    pub success: bool,
    #[serde(rename = "errorCode", skip_serializing_if = "String::is_empty")]
    pub error_code: String,
    #[serde(rename = "errorMessage", skip_serializing_if = "String::is_empty")]
    pub error_message: String,
    pub data: Option<T>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ListResult<T> {
    pub success: bool,
    #[serde(rename = "errorCode", skip_serializing_if = "String::is_empty")]
    pub error_code: String,
    #[serde(rename = "errorMessage", skip_serializing_if = "String::is_empty")]
    pub error_message: String,
    pub data: Option<Vec<T>>,
    pub total: i64,
}

// {"key":"2","label":"辣~"}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UserTag {
    pub key: String,
    pub label: String,
}

// {"label": "浙江省", "key": "330000"}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct LabelKey {
    pub label: String,
    pub key: String,
}


#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Geographic {
    pub province: LabelKey,
    pub city: LabelKey,
}

// {"success":true,"data":{"name":"Serati Ma","avatar":"https://gw.alipayobjects.com/zos/antfincdn/XAosXuNZyF/BiazfanxmamNRoxxVxka.png","userid":"00000001","email":"antdesign@alipay.com","signature":"海纳百川，有容乃大","title":"交互专家","group":"蚂蚁金服－某某某事业群－某某平台部－某某技术部－UED","tags":[{"key":"0","label":"很有想法的"},{"key":"1","label":"专注设计"},{"key":"2","label":"辣~"},{"key":"3","label":"大长腿"},{"key":"4","label":"川妹子"},{"key":"5","label":"海纳百川"}],"notifyCount":12,"unreadCount":11,"country":"China","access":"admin","geographic":{"province":{"label":"浙江省","key":"330000"},"city":{"label":"杭州市","key":"330100"}},"address":"西湖区工专路 77 号","phone":"0752-268888888"}}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct CurrentUser {
    pub name: String,
    pub avatar: String,
    pub userid: String,
    pub email: String,
    pub signature: String,
    pub title: String,
    pub group: String,
    pub tags: Vec<UserTag>,
    #[serde(rename = "notifyCount")]
    pub notify_count: u32,
    #[serde(rename = "unreadCount")]
    pub unread_count: u32,
    pub country: String,
    pub access: String,
    pub address: String,
    pub phone: String,
    pub geographic: Geographic,
}