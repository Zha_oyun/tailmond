
/**
 * 白名单
 */
use actix_web::{get, delete, post, web, Error, HttpResponse};
use chrono::Duration;
use chrono::Local;
use md5::Md5;
use md5::Digest;
use crate::model::allow_list_model::*;
use super::DbPool;
use crate::models::{self};
use crate::action;

/**
 * 分页查询
 */
#[post("/api/allow-list/query/find-by-page")]
async fn find_by_page(
    pool: web::Data<DbPool>,
    form: web::Json<AllowListDTO>,
) -> Result<HttpResponse, Error> {
    // use web::block to offload blocking Diesel code without blocking server thread
    if form.current.is_none() || form.page_size.is_none() {
        let list_result: models::ListResult<AllowList> = models::ListResult {
            success: true,
            error_code: "".to_string(),
            error_message: "current或pageSize不能为空".to_string(),
            data: None,
            total: 0,
        };
        return Ok(HttpResponse::Ok().json(list_result));
    }
    let alert_vec = web::block(move || {
        let conn = pool.get()?;
        action::allow_list_action::find_by_page(form.current.unwrap(), form.page_size.unwrap(), &conn)
    })
    .await?
    .map_err(actix_web::error::ErrorInternalServerError)?;

    Ok(HttpResponse::Ok().json(models::ListResult {
        success: true,
        error_code: "".to_string(),
        error_message: "".to_string(),
        data: alert_vec.0,
        total: alert_vec.1,
    }))
}

/**
 * 按ID查询
 */
#[post("/api/allow-list/query/find-by-id")]
async fn find_by_id(
    pool: web::Data<DbPool>,
    form: web::Json<AllowListDTO>,
) -> Result<HttpResponse, Error> {
    if form.id.is_none() {
        let sys_result: models::SysResult<AllowList> = models::SysResult {
            success: true,
            error_code: "".to_string(),
            error_message: "id不能为空".to_string(),
            data: None,
        };
        return Ok(HttpResponse::Ok().json(sys_result));
    }
    // use web::block to offload blocking Diesel code without blocking server thread
    let allow_list = web::block(move || {
        let conn = pool.get()?;
        action::allow_list_action::find_by_id(form.id.as_ref().unwrap(), &conn)
    })
    .await?
    .map_err(actix_web::error::ErrorInternalServerError)?;

    Ok(HttpResponse::Ok().json(models::SysResult {
        success: true,
        error_code: "".to_string(),
        error_message: "".to_string(),
        data: Some(allow_list),
    }))
}

/**
 * 新增
 */
#[post("/api/allow-list/insert")]
async fn insert(
    pool: web::Data<DbPool>,
    form: web::Json<AllowListDTO>,
) -> Result<HttpResponse, Error> {
    if form.allow_type.is_none() || form.allow_key.is_none() {
        let sys_result: models::SysResult<AllowList> = models::SysResult {
            success: true,
            error_code: "".to_string(),
            error_message: "allow_type或allow_key不能为空".to_string(),
            data: None,
        };
        return Ok(HttpResponse::Ok().json(sys_result));
    }
    // use web::block to offload blocking Diesel code without blocking server thread
    
    let allow_list_vec = web::block(move || {
        let conn = pool.get()?;
        
        let mut hasher = Md5::new();
        hasher.update(form.allow_type.as_ref().unwrap().as_bytes());
        hasher.update(form.allow_key.as_ref().unwrap().as_bytes());
        let result = hasher.finalize();
        let id = format!("{:x}", result);

        let create_time = Local::now().timestamp_millis();
        let mut expire_time = Local::now().checked_add_signed(Duration::days(365)).unwrap().timestamp_millis();
        if form.expire_time.is_some() {
            expire_time = form.expire_time.unwrap();
        }

        action::allow_list_action::insert(AllowList {
            id: id,
            allow_type: form.allow_type.as_ref().unwrap().to_string(),
            allow_key: form.allow_key.as_ref().unwrap().to_string(),
            create_time: create_time,
            expire_time: expire_time,
            source: "user".to_string()
        }, &conn)
    })
    .await?
    .map_err(actix_web::error::ErrorInternalServerError)?;

    Ok(HttpResponse::Ok().json(models::SysResult {
        success: true,
        error_code: "".to_string(),
        error_message: "".to_string(),
        data: Some(allow_list_vec),
    }))
}

/**
 * 删除
 */
#[delete("/api/allow-list/delete")]
async fn delete(
    pool: web::Data<DbPool>,
    form: web::Json<AllowListDTO>,
) -> Result<HttpResponse, Error> {
    if form.id.is_none() {
        let sys_result: models::SysResult<AllowList> = models::SysResult {
            success: true,
            error_code: "".to_string(),
            error_message: "id不能为空".to_string(),
            data: None,
        };
        return Ok(HttpResponse::Ok().json(sys_result));
    }
    // use web::block to offload blocking Diesel code without blocking server thread
    web::block(move || {
        let conn = pool.get()?;
        action::allow_list_action::delete_by_id(form.id.as_ref().unwrap(), &conn)
    })
    .await?
    .map_err(actix_web::error::ErrorInternalServerError)?;

    Ok(HttpResponse::Ok().json(models::SysResult {
        success: true,
        error_code: "".to_string(),
        error_message: "".to_string(),
        data: Some(()),
    }))
}