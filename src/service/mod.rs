use diesel::{r2d2::{self, ConnectionManager}, SqliteConnection};

pub mod user_service;
pub mod alert_service;
pub mod allow_list_service;
pub mod recovery_list_service;
pub mod alert_prompt_ws;
pub mod settings_service;

type DbPool = r2d2::Pool<ConnectionManager<SqliteConnection>>;