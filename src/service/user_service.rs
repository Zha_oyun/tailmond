use actix_web::{get, web, Error, HttpResponse};
use serde_json::json;
use crate::{DbPool, actions};

/// Finds user by UID.
#[get("/api/alert/{id}")]
async fn get_user(
    pool: web::Data<DbPool>,
    id: web::Path<String>,
) -> Result<HttpResponse, Error> {
    let id = id.into_inner();
    let alert_id = id.clone();

    // use web::block to offload blocking Diesel code without blocking server thread
    let alert = web::block(move || {
        let conn = pool.get()?;
        actions::find_alert_by_id(alert_id, &conn)
    })
    .await?
    .map_err(actix_web::error::ErrorInternalServerError)?;

    if let Some(alert) = alert {
        Ok(HttpResponse::Ok().json(alert))
    } else {
        let res = HttpResponse::NotFound().body(format!("No user found with uid: {}", id));
        Ok(res)
    }
}


#[get("/api/project/notice")]
async fn notice() -> Result<HttpResponse, Error> {
    Ok(HttpResponse::Ok().json(json!({
        "data": [
        //   {
        //     "id": "xxx1",
        //     "title": "titles[0]",
        //     "logo": "/user.svg",
        //     "description": "那是一种内在的东西，他们到达不了，也无法触及的",
        //     "updatedAt": "2022-5-20 22:26:53",
        //     "member": "科学搬砖组",
        //     "href": "",
        //     "memberLink": ""
        //   }
        ],
      })))
}


#[get("/api/notices")]
async fn notices() -> Result<HttpResponse, Error> {
    Ok(HttpResponse::Ok().json(json!({
        "data": [
          {
            "id": "000000001",
            "avatar": "https://gw.alipayobjects.com/zos/rmsportal/ThXAXghbEsBCCSDihZxY.png",
            "title": "你收到了 14 份新周报",
            "datetime": "2017-08-09",
            "type": "notification"
          },
          {
            "id": "000000002",
            "avatar": "https://gw.alipayobjects.com/zos/rmsportal/OKJXDXrmkNshAMvwtvhu.png",
            "title": "你推荐的 曲妮妮 已通过第三轮面试",
            "datetime": "2017-08-08",
            "type": "notification"
          }
        ]
      })))
}

#[get("/api/activities")]
async fn activities() -> Result<HttpResponse, Error> {
    Ok(HttpResponse::Ok().json(json!({
        "data": [
        //   {
        //     "id": "trend-1",
        //     "updatedAt": "2022-5-20 22:23:48",
        //     "user": {
        //       "name": "内核监控",
        //       "avatar": "/user.svg"
        //     },
        //     "group": {
        //       "name": "Fa",
        //       "link": "http://github.com/"
        //     },
        //     "project": {
        //       "name": "a",
        //       "link": "http://github.com/"
        //     },
        //     "template": "在 @{group} 新建项目 @{project}"
        //   }
        ]
      })))
}

#[get("/api/fake_workplace_chart_data")]
async fn fake_workplace_chart_data() -> Result<HttpResponse, Error> {
    Ok(HttpResponse::Ok().json(json!({
        "data": {
            "visitData": [
                {
                    "x": "2022-05-20",
                    "y": 7
                },
                {
                    "x": "2022-05-21",
                    "y": 5
                }
            ],
            "visitData2": [
                {
                    "x": "2022-05-20",
                    "y": 1
                },
                {
                    "x": "2022-05-21",
                    "y": 6
                }
            ],
            "salesData": [
                {
                    "x": "1月",
                    "y": 311
                },
                {
                    "x": "2月",
                    "y": 573
                }
            ],
            "searchData": [
                {
                    "index": 1,
                    "keyword": "搜索关键词-0",
                    "count": 435,
                    "range": 33,
                    "status": 0
                },
                {
                    "index": 2,
                    "keyword": "搜索关键词-1",
                    "count": 453,
                    "range": 22,
                    "status": 1
                }
            ],
            "offlineData": [
                {
                    "name": "Stores 0",
                    "cvr": 0.8
                },
                {
                    "name": "Stores 1",
                    "cvr": 0.4
                }
            ],
            "offlineChartData": [
                {
                    "date": "02:21",
                    "type": "客流量",
                    "value": 64
                },
                {
                    "date": "02:21",
                    "type": "支付笔数",
                    "value": 80
                }
            ],
            "salesTypeData": [
                {
                    "x": "家用电器",
                    "y": 4544
                },
                {
                    "x": "食用酒水",
                    "y": 3321
                }
            ],
            "salesTypeDataOnline": [
                {
                    "x": "家用电器",
                    "y": 244
                },
                {
                    "x": "食用酒水",
                    "y": 321
                }
            ],
            "salesTypeDataOffline": [
                {
                    "x": "家用电器",
                    "y": 99
                },
                {
                    "x": "食用酒水",
                    "y": 188
                }
            ],
            "radarData": [
                {
                    "name": "个人",
                    "label": "引用",
                    "value": 10
                },
                {
                    "name": "个人",
                    "label": "口碑",
                    "value": 8
                }
            ]
        }
    })))
}