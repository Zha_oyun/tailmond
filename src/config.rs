
use std::sync::Arc;
use config::Config;
use std::collections::HashMap;

pub trait ConfigAction {

    fn init(config_path: &str) -> Self;

    fn get(&self, k: &str) -> Option<&String>;
}

#[derive(Clone)]
pub struct LocalConfig {
    config: Arc<HashMap<String, String>>,
}

impl ConfigAction for LocalConfig {

    fn init(config_path: &str) -> Self {

        let settings = Config::builder()
            // Add in `./Settings.toml`
            .add_source(config::File::with_name(config_path))
            // Add in settings from the environment (with a prefix of APP)
            // Eg.. `APP_DEBUG=1 ./target/app` would set the `debug` key
            .add_source(config::Environment::with_prefix("TAILMOND_"))
            .build()
            .unwrap();

        LocalConfig { config: Arc::new(settings.try_deserialize::<HashMap<String, String>>().unwrap()) }
    }

    fn get(&self, k: &str) -> Option<&String> {
        self.config.get(k)
    }

}