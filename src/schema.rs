table! {
    alerts (id) {
        id -> Text,
        severity -> Text,
        category -> Text,
        description -> Text,
        time -> BigInt,
        source -> Text,
        mal_obj_type -> Text,
        mal_obj_key -> Text,
        mal_obj_country -> Text,
        mal_obj_city -> Text,
        rule_id -> Text,
        solved -> Bool,
    }
}

table! {
    allow_lists (id) {
        id -> Text,
        allow_type -> Text,
        allow_key -> Text,
        create_time -> BigInt,
        expire_time -> BigInt,
        source -> Text,
    }
}

table! {
    recovery_lists (id) {
        id -> Text,
        mal_obj_key -> Text,
        mal_obj_type -> Text,
        recovery_key -> Text,
        recovery_value -> Text,
        recovery_type -> Text,
        create_time -> BigInt,
        expire_time -> BigInt,
        source -> Text,
    }
}

table! {
    settings (id) {
        id -> Text,
        value -> Text,
    }
}

allow_tables_to_appear_in_same_query!(
    alerts,
    allow_lists,
    recovery_lists,
    settings,
);
